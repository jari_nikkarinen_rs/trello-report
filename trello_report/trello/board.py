import json
import csv
from datetime import datetime

class Board:
    b = {} # full json dictionary   

    def SerializeJson(self):
        return json.dumps(self.b)

    def LoadJson(self, j):
        board = {}
        with open(j, encoding="utf8") as jsonFile:
            board = json.load(jsonFile)
            jsonFile.close()
        return board

    def GetListCounts(self):
        result = []
        for list in self.b['lists']:
            count = 0
            for card in self.b['cards']:
                if card['idList'] == list['id'] and card['closed'] == False:
                    count += 1
            if list['closed'] == False:
                result.append([list['name'], count])
        
        # print(result)
        return result

    def GetAverageCardMoveTimes(self, list_pairs):
        result = []
        for pair in list_pairs:
            r = self.GetAverageCardMoveTime(pair['from_list'], pair['to_list'])
            result.append(r)
        # print(result)
        return result

    def GetAverageCardMoveTime(self, from_list, to_list):
        
        
        # find ids
        # to_id = ""
        from_id = ""
        for list in self.b['lists']:
            if from_list in list['name']:
                from_id = list['id']
                
        #     if to_list in list['name']:
        #         to_id = list['id']
                
        # print(from_id)
        # print(to_id)
        count = 0
        sum = 0
        for action in self.b['actions']:
            # print(action['type'])
            if (action['type'] == "createCard" or action['type'] == "copyCard") and action['data']['list']['id'] == from_id:
                # print(action['data']['card']['name'] + " --- " + action['data']['list']['name'] + " --- " + action['type'] + " --- " + action['date'] )
                card_id = action['data']['card']['id']
                # print(card_id)
                for action2 in self.b['actions']:
                    if action2['type'] == "updateCard" and card_id == action2['data']['card']['id'] and action2['data'].get('listAfter') != None  and to_list in action2['data']['listAfter']['name']:
                        # print("to " + action2['data']['card']['name'] + " --- " + action2['data']['listAfter']['name'] + " --- " + action2['type'] + " --- " + action2['data']['card']['id'] )
                        count += 1
                        delta = datetime.fromisoformat(action2['date'][:-1]) - datetime.fromisoformat(action['date'][:-1]) # just took the last mark of the date string ('Z')
                        # print(delta.total_seconds())
                        sum += delta.total_seconds() /60 /60 /24
                    # if action2['type'] == "updateCard" and card_id == action2['data']['card']['id']:
                    #     print("updates " + str(action2['type'])  + "  " +  str(action2['data']) )
                # print(" --- ")
        avrg = 0
        if count != 0:
            avrg = sum / count
        # print(str(count) + ", " + str(sum)+ ", " + str(avrg))
        # print(result)
        return [from_list, to_list, count, avrg ]

    
    def WriteCsv(self, name, data, headers):
        with open(name + ".csv", 'w', encoding='UTF8', newline='') as f:
            writer = csv.writer(f)
            writer.writerow(headers)
            for d in data:
                writer.writerow(d)