from trello import board
# from trello import file
import sys


def main():
    if(sys.argv[1] != None):
        file = sys.argv[1]
        # print(sys.argv[1])
        trello_board = board.Board()
        trello_board.b = trello_board.LoadJson(file)
        
        # for card in trello_board.b['cards']:
        #     print(card['name'] + "\n")

        header = ['From List', 'To List', 'Moved Card Count', 'Average Days']
        counts = trello_board.GetListCounts()
        trello_board.WriteCsv("Counts", counts, header)

        header = ['From List', 'To List', 'Moved Card Count', 'Average Days']
        operations = trello_board.LoadJson('operations.json')
        averages = trello_board.GetAverageCardMoveTimes(operations['average_operation'])
        trello_board.WriteCsv("Averages", averages, header)
        
        # print(trello_board.b)
    else:
        print("Commandline parameter missing")

if __name__ == '__main__':
    main()




