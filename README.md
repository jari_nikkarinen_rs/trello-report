# Trello Report V 0.1 #

Tool for getting averages of how long does it take for a card to move from desired list to a another. This tool is used to read boards json that can be exported from Trello. Only the cards that have "createCard" or "copyCard" actions and are moved between desired list and tracked.

### Requirements ###

* Python 3.9


### Usage ###

* Edit operations.json to hold desired from and to lists. to_list name is considered inclusive.
~~~~
{
    "start_date":"",
    "end_sate":"",
    "average_operation":[
        {
            "from_list": "Development requests / Ideas",
            "to_list": "Release"
        },
        {
            "from_list": "Issues",
            "to_list": "Release"
        }
    ]
}
~~~~

* Commands. 
~~~~
python trello_report.py your_json.json
~~~~

* Tool creates two csv files as a result. Counts.csv and Averages.csv.


### Other Stuff ###

* Todo

